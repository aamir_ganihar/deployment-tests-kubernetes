// +build kubeall kubernetes
// NOTE: See the notes in the other Kubernetes example tests for why this build tag is included.

package test

import (
	"fmt"
	"testing"
	"time"
        "crypto/tls"

	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
        "github.com/gruntwork-io/terratest/modules/k8s"
)

func TestConduitFrontEndServiceExample(t *testing.T) {
	t.Parallel()

	// Path to the Conduit frontend Kubernetes deployment resource config file
	//kubeDeploymentPath := "conduit-frontend-deployment.yaml"

        // Path to the Conduit frontend Kubernetes service file
        //kubeServicePath := "conduit-frontend-service.yaml"

	// Setup the kubectl config and context.
	options := k8s.NewKubectlOptions("", "", "default")

	// At the end of the test, run "kubectl delete" to clean up any resources that were created.
	// defer k8s.KubectlDelete(t, options, kubeDeploymentPath)
        // defer k8s.KubectlDelete(t, options, kubeServicePath)

	// Run `kubectl apply` to deploy. Fail the test if there are any errors.
	//k8s.KubectlApply(t, options, kubeDeploymentPath)
        //k8s.KubectlApply(t, options, kubeServicePath)

	// Verify the service is available and get the URL for it.
	//k8s.WaitUntilServiceAvailable(t, options, "conduit-frontend-lb", 30, 1*time.Second)
        service := k8s.GetService(t, options, "conduit-frontend-lb")
	endpoint := k8s.GetServiceEndpoint(t, options, service, 80)
 
        //Make an HTTP request to the URL and make sure it returns a 200 OK
        tlsConfig := tls.Config{}
   
        http_helper.HttpGetWithRetryWithCustomValidation(
                t,
                fmt.Sprintf("http://%s", endpoint),
                &tlsConfig,
                30,
                10*time.Second,
                func(statusCode int, body string) bool {
                        return statusCode == 200
                },
        ) 
}

