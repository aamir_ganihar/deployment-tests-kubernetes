module conduit

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gruntwork-io/terratest v0.24.2
)
