// +build kubeall kubernetes
// NOTE: See the notes in the other Kubernetes example tests for why this build tag is included.

package test

import (
        "fmt"
        "time"
	"testing"
        "database/sql"
        _ "github.com/go-sql-driver/mysql"
        "github.com/gruntwork-io/terratest/modules/k8s"
)

func TestConduitMysqlService(t *testing.T) {
	t.Parallel()
        
        // Path to the Conduit Mysql Kubernetes persistent volume config file
        //kubePersistentVolumePath := "conduit-mysql-pv.yaml"

	// Path to the Conduit Mysql Kubernetes deployment resource config file
	//kubeDeploymentPath := "conduit-mysql-deployment.yaml"

        // Path to the Conduit Mysql Kubernetes service file
        //kubeServicePath := "conduit-mysql-service.yaml"

	// Setup the kubectl config and context.
	options := k8s.NewKubectlOptions("", "", "default")

	// At the end of the test, run "kubectl delete" to clean up any resources that were created.
        // defer k8s.KubectlDelete(t, options, kubePersistentVolumePath)
	// defer k8s.KubectlDelete(t, options, kubeDeploymentPath)
        // defer k8s.KubectlDelete(t, options, kubeServicePath)

	// Run `kubectl apply` to deploy. Fail the test if there are any errors.
        //k8s.KubectlApply(t, options, kubePersistentVolumePath)
	//k8s.KubectlApply(t, options, kubeDeploymentPath)
        //k8s.KubectlApply(t, options, kubeServicePath)

	// Verify the service is available and query the Mysql database.
	k8s.WaitUntilServiceAvailable(t, options, "mysql-service", 30, 1*time.Second)

        var mysql_username = "root"
        var mysql_pwd = "Aamir123@"
        var mysql_host = "192.168.64.4"
        var mysql_port = "31102"
        var mysql_database = "conduit"
        
        uri := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", mysql_username, mysql_pwd, mysql_host, mysql_port, mysql_database)
 
        con, err := sql.Open("mysql", uri)
        if err != nil {
                panic(err.Error())
        }
        defer con.Close()
        
        // Insert a value into the Mysql database       
        var test_id = "1234" 
        var username = "test_mysql"
        var pwd = "test_mysql_pwd"
        var email = "test_mysql@test.com"

        stmt, err := con.Prepare("INSERT INTO users(id, username, password, email) VALUES(?, ?, ?, ?)")         
        if err != nil {
		panic(err.Error())
	}
	defer stmt.Close() 

        res, err := stmt.Exec(test_id, username, pwd, email)
        if err != nil {
                panic(err.Error())
        }
    
        id, _ := res.LastInsertId()

        fmt.Printf("Inserted row: %d", id)
}

